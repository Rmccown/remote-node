This is the remote node for my OpenLCB / ArduinoCMRI one-to-many node.  By default it will drive 4 pair of red/green signals. it is meant to sit at each interlocking and provide signaling support.

It was written to run on an Arduino Pro Micro.  There is no reason it wont run on any other Arduino and provide more IO pins.